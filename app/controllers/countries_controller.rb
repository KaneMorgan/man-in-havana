class CountriesController < ApplicationController

  def index
    render json: Countries.all.to_json
  end
end
