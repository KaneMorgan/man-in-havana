class DiplomatsController < ApplicationController
  def index
    render json: Diplomat.all.to_json
  end

  def roles
    render json: Role.all.to_json
  end

  def show

    @diplomat = Diplomat.find( params["id"])
  end

  def list
    search_term = params['q']
    if search_term
      @diplomats = Diplomat.where(" UPPER(name) like ?", '%' + search_term.upcase + '%')
    else
      @diplomats = Diplomat.all
    end
  end
end
