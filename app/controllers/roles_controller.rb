class RolesController < ApplicationController

  def index
    @diplomat = Diplomat.find params["diplomat_id"]
    render json: @diplomat.roles.map(&:to_json)
  end

  def list
    @roles = Role.all
  end

  def show

    @siblings = Role.find(params['id']).siblings
    dates =
      @siblings
        .select {|s| s.start_date && s.end_date }
        .map{ |s| s.end_date.to_f - s.start_date.to_f }
    @tenure = dates.sum / dates.length
  end
end
