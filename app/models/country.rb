class Country < ApplicationRecord
  has_many :diplomats, through: :roles
end
