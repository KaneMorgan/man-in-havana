class CountryRelation < ApplicationRecord
  belongs_to :country
  belongs_to :diplomat
end
