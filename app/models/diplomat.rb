class Diplomat < ApplicationRecord
  has_many :roles

  def contemporaries
    Diplomat.where("born between '?' and '?'", born.to_i - 5, born.to_i + 5)
  end
end
