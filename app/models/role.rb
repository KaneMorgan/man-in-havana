class Role < ApplicationRecord
  belongs_to :diplomat


  def siblings
    Role.where(role_name: role_name)
  end
end
