class XmlLoader

  def initialize
    @doc = File.open("foo.xml") { |f| Nokogiri::XML(f) }
  end

  def load_section_1_entries
    section_1_entries
      .select{ |entry| entry.css('date').first }
      .map do |entry|
      # There are 46 entries in the data where there is no date for the person
      # and instead there is a reference to another name, I don't know what this is
      # supposed to represent so I'm excluding those.

        diplomat = Diplomat.create({
          name: entry.css('persName').text,
          born: entry.css('date').first.attribute('from').try(:value),
          died: entry.css('date').first.attribute('to').try(:value)
        })

        roles = entry.css('seg').map do |seg|
          Role.create({
            role_name: seg.css('rs').text,
            start_date: seg.css('date').first.attribute('from').try(:value),
            end_date: seg.css('date').first.attribute('to').try(:value),
            diplomat: diplomat
          })
        end
    end
  end

  def section_1_entries
    @section_1_entries ||= @doc.css('div[type="entry_list"]').css('entry')
  end

  def load_section_2_entries
    diplomats = Diplomat.all

    countries.each do |c|
      # c.css('head').first}.compact.map{|h| Country.create name:h.text }
      diplomat_names = c.css('persName').text
      diplomat_names.each do |n|
        chunks = n.split(' ').select { |na| na.size > 3 }
        chunks.each do |chunk|
          possibles = diplomats.select { |d| d.name.include? chunk }

        end
      end
    end
  end

  def section_2_entries
    @section_2_entries ||= @doc.css('div[type="countries_section"]').css('div[type="subsection"]').first
  end

  ## gets the div node that represents a country
  def country_heads
    section_2_entries
      .children
      .select{|d| d.name='div'}
      .map{|n| n.css('head')}
      .reject{|f| f.empty? }
      .map{ |n| n.first.parent}
  end

  def countries
    a = country_heads.first
    @countries = []
    while a do
      a = a.next_element
      @countries << a if a && a.name =='div'
    end
    @countries
  end
end
