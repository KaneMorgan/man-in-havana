Rails.application.routes.draw do
  resources :countries
  resources :diplomats do
    resources :roles
  end

  get '/roles', to: 'roles#list'
  root 'diplomats#list'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
