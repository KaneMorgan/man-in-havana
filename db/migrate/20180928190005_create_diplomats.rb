class CreateDiplomats < ActiveRecord::Migration[5.1]
  def change
    create_table :diplomats do |t|
      t.string :name
      t.string :born
      t.string :died
      t.timestamps
    end
  end
end
