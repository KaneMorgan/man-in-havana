class CreateRoles < ActiveRecord::Migration[5.1]
  def change
    create_table :roles do |t|
      t.integer :diplomat_id
      t.string :role_name
      t.string :start_date
      t.string :end_date
      t.timestamps
    end
  end
end
