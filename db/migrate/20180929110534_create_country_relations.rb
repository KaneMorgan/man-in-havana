class CreateCountryRelations < ActiveRecord::Migration[5.1]
  def change
    create_table :country_relations do |t|
      t.integer :country_id
      t.integer :diplomat_id
    end
  end
end
